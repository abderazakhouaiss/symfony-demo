<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, ['label' => false, 'attr' => ['class' => 'form-control m-2', 'placeholder' => 'First name']])
            ->add('lastName', null, ['label' => false, 'attr' => ['class' => 'form-control m-2', 'placeholder' => 'Last name']])
            ->add('email', EmailType::class, ['label' => false, 'attr' => ['class' => 'form-control m-2', 'placeholder' => 'Email', 'pattern' => '^[a-z0-9][a-z0-9._-]*@[a-z0-9_-]{2,}(\.[a-z]{2,4}){1,2}$']])
            ->add('birthDay', BirthdayType::class, ['widget' => 'single_text', 'attr' => ['class' => 'form-control m-2']])
            ->add('gender', ChoiceType::class, [
                'attr' => ['class' => 'form-control m-2'],
                'label' => false,
                'mapped' => false,
                'choices' => [
                    'Gender' => 'gender',
                    'Male' => 'male',
                    'Female' => 'female',
                ],
                'choice_attr' => function ($val, $key, $index) {
                    return $key === 'gender' ? ['disabled' => 'disabled', 'selected' => 'selected'] : [];
                },
            ])
            ->add('phone', null, ['label' => false, 'attr' => ['class' => 'form-control m-2', 'placeholder' => 'Phone']])
            ->add('pays', CountryType::class, ['label' => false, 'attr' => ['class' => 'form-control m-2', 'placeholder' => 'Country']])
            ->add('plainPassword', PasswordType::class, [
                'label' => false,
                'mapped' => false,
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a password',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
                'attr' => ['class' => 'form-control m-2', 'placeholder' => 'Password']
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
