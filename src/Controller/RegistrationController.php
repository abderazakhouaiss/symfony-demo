<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Service\UserUtils;
use DateTime;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class RegistrationController extends AbstractController
{
    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request, UserUtils $userUtils, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = new User();
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);
        $user->setRegisterDate(new \DateTime());
        $checkConditions = [];

        // Get result messages from GET request.
        $messages = $request->get('messages');

        if ($form->isSubmitted()) {
            $checkConditions = $userUtils->checkRegisterConditions($user);
            if ($form->isValid() && $checkConditions['success']) {
                // Encode the plain password.
                $user->setPassword($passwordEncoder->encodePassword($user,$form->get('plainPassword')->getData()));
                $userUtils->setUserStatus($user);
                $entityManager = $this->getDoctrine()->getManager();
                try {
                    $entityManager->persist($user);
                    $entityManager->flush();
                    $messages = ['User created successfully'];
                } catch (Exception $e) {
                    $messages = ['Record not saved: Error while saving to DB!'];
                }

                return $this->redirectToRoute('app_register', [
                    'messages' => $messages
                ]);
            }
        }
        $messages = !empty($checkConditions['message']) ? [$checkConditions['message']] : $messages;
        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
            'messages' => $messages
        ]);
    }

    /**
     * @Route("/import", name="user_import")
     * @throws Exception
     */
    public function importUsers(Request $request, UserUtils $userUtils): Response
    {
        $importMessage = ['Csv file is empty or not valid'];
        if (empty($_FILES['csv_file'])) {
            return $this->redirectToRoute('app_register', [
                'messages' => $importMessage
            ]);
        }
        $tmpName = $_FILES['csv_file']['tmp_name'];
        $csvAsArray = array_map('str_getcsv', file($tmpName));
        if (!empty($csvAsArray)) {
            $header = $csvAsArray[0];
            unset($csvAsArray[0]);
            $logs = [];
            foreach ($csvAsArray as $key => $item) {
                $datum = array_combine($header, $item);
                $logs[] = $key . ' => ' . $userUtils->saveUser($datum);
            }
            $importMessage = $logs;
        }
        return $this->redirectToRoute('app_register', [
            'messages' => $importMessage
        ]);
    }
}
