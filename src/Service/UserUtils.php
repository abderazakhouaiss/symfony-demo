<?php


namespace App\Service;


use App\Entity\User;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserUtils
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * UserUtils constructor.
     * @param EntityManagerInterface $entityManager
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(EntityManagerInterface $entityManager, UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->entityManager = $entityManager;
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * Check given conditions before saving user to DB.
     *
     * @param User $user
     *   User entity.
     *
     * @return array
     *   Returns true if Ok, false otherwise.
     */
    public function checkRegisterConditions(User $user) : array
    {
        // Check user age and nationality.
        $age = $user->getBirthDay()
            ->diff(new DateTime('now'))
            ->y;
        if ($user->getPays() === 'MA' && $age < 16) {
            return [
                'success' => false,
                'message' => 'Age should be older than 16yo!',
            ];
        } elseif ($user->getPays() !== 'MA' && $age < 18) {
            return [
                'success' => false,
                'message' => 'Age should be older than 18yo!',
            ];
        }

        // Check the total of inscriptions.
        /** @var User[] $users */
        $users = $this->entityManager->getRepository(User::class)->findAll();
        if (count($users) > 11) {
            $sumAges = 0;
            foreach ($users as $obj) {
                $objAge = $obj->getBirthDay()
                    ->diff(new DateTime('now'))
                    ->y;
                $sumAges += $objAge;
            }
            $sumAges = $sumAges / count($users);
            if ($age < ($sumAges - 0.1) || $age > ($sumAges + 0.1)) {
                return [
                    'success' => false,
                    'message' => 'Age is not valid!',
                ];
            }
        }

        return [
            'success' => true,
        ];
    }

    public function setUserStatus(User &$user) {
        // Check inscription date.
        $registerHour = $user->getRegisterDate()->format('H');
        if ($registerHour < 12 || $registerHour > 21) {
            $user->setStatus(false);
        } else {
            $user->setStatus(true);
        }
    }

    /**
     * @throws Exception
     */
    public function saveUser(array $data): string
    {
        $user = new User();
        $date = new DateTime($data['birthday'] ?? 'now');
        $user->setRegisterDate(new \DateTime());
        $user->setName($data['first_name'] ?? '');
        $user->setLastName($data['last_name'] ?? '');
        $user->setEmail($data['email'] ?? '');
        $user->setBirthDay($date);
        $user->setGender($data['gender']);
        $user->setPhone($data['phone']);
        $user->setPays($data['country']);
        $user->setPassword(
            $this->passwordEncoder->encodePassword(
                $user,
                $data['password'] ?? $data['phone']
            )
        );
        $isUserValid = $this->checkRegisterConditions($user);
        if (!$isUserValid['success']) {
            return 'Record not saved: ' . $isUserValid['message'];
        }
        $this->setUserStatus($user);
        try {
            $this->entityManager->persist($user);
            $this->entityManager->flush();
        } catch (Exception $e) {
            return 'Record not saved: Error while saving to DB!';
        }
        return 'Record Saved!';
    }

}